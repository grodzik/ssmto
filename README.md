# SSMTO

## Sign in to any of your EC2 instance with ease

`ssmto` is using AWS Session Manager to connect to EC2 instances.
This requires valid AWS CLI credentials on local machine and SSM agent running
on EC2 instance with proper instance profile permissions attached to it

## Dependencies

* [slmenu](https://bitbucket.org/rafaelgg/slmenu/src/default/)
* [session-manager-plugin](https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager-working-with-install-plugin.html)

## Usage

```
user: ssmto
```
